<?php

namespace App\Controller;
use App\Service\Form;
use App\Service\Validation;
use App\Model\ContactModel;
use Core\Kernel\Config;




class ContactController extends BaseController {

  public function index()
    {
        $message = 'vous etes sur la page contact';

        $contact = ContactModel::All();

        $this->dump($contact);
        $this->render('app.contact.index',array(
            'message' => $message,
            'contact' => ContactModel::ContactOrderBy(),
        ));
    }

    public function add() {
        $errors = [];

        if(!empty($_POST['submitted'])) {

            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            
            if($v->isValid($errors))  {
                ContactModel::insert($post);
                $this->addFlash('success', 'Merci pour votre message');
                $this->redirect('Contact');
            }
        }


        $form = new Form($errors);
        $this->render('app.contact.add',array(
            'form' => $form
        ));
    }


    private function validate($v,$post)
    {
        $errors = [];
        $errors['sujet'] = $v->textValid($post['sujet'], 'sujet',2, 100);
        $errors['email'] = $v->emailValid($post['email'], 'email',5, 500);
        $errors['message'] = $v->textValid($post['message'], 'message',5, 250);
        return $errors;
    }

}