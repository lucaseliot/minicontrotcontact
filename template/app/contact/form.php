<div class="wrapform">
    <div class="form">
        <form action="" method="post" novalidate>
            <?php echo $form->label('sujet'); ?>
            <?php echo $form->input('sujet', 'text', $Contact->sujet ?? ''); ?>
            <?php echo $form->error('sujet'); ?>

            <?php echo $form->label('email'); ?>
            <?php echo $form->input('email', 'email', $Contact->email ?? ''); ?>
            <?php echo $form->error('email'); ?>

            <?php echo $form->label('message'); ?>
            <?php echo $form->textarea('message', $Contact->message ?? ''); ?>
            <?php echo $form->error('message'); ?>

            <?php echo $form->submit('submitted', $textButton); ?>
        </form>
    </div>
</div>
